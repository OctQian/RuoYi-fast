# 使用官方的Java 8作为基础镜像
FROM openjdk:8-jdk-alpine

# 设置工作目录
WORKDIR /app

# 复制应用程序的JAR文件到工作目录
COPY ./ruoyi.jar /app/ruoyi.jar

# 如果应用程序依赖外部配置文件，也可以将配置文件复制到工作目录
# COPY ./config/application.properties /app/config/application.properties

# 暴露应用程序的端口（如果有需要）
# EXPOSE 8080

# 设置Java虚拟机参数（可选）
# ENV JAVA_OPTS="-Xmx512m -Xms256m"

# 容器启动时运行的命令
CMD ["java", "-jar", "ruoyi.jar"]

# 或者，如果应用程序使用Spring Boot并包含了嵌入式的Web服务器，则可以使用以下命令启动：
# CMD ["java", "-Dspring.profiles.active=prod", "-jar", "your-application.jar"]
