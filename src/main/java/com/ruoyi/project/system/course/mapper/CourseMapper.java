package com.ruoyi.project.system.course.mapper;

import java.util.List;
import com.ruoyi.project.system.course.domain.Course;

/**
 * 学生签到Mapper接口
 * 
 * @author ruoyi
 * @date 2024-04-22
 */
public interface CourseMapper 
{
    /**
     * 查询学生签到
     * 
     * @param id 学生签到主键
     * @return 学生签到
     */
    public Course selectCourseById(Long id);

    /**
     * 查询学生签到列表
     * 
     * @param course 学生签到
     * @return 学生签到集合
     */
    public List<Course> selectCourseList(Course course);

    /**
     * 新增学生签到
     * 
     * @param course 学生签到
     * @return 结果
     */
    public int insertCourse(Course course);

    /**
     * 修改学生签到
     * 
     * @param course 学生签到
     * @return 结果
     */
    public int updateCourse(Course course);

    /**
     * 删除学生签到
     * 
     * @param id 学生签到主键
     * @return 结果
     */
    public int deleteCourseById(Long id);

    /**
     * 批量删除学生签到
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCourseByIds(String[] ids);
}
