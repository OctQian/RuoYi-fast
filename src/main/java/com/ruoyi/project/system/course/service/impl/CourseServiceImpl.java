package com.ruoyi.project.system.course.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.system.course.mapper.CourseMapper;
import com.ruoyi.project.system.course.domain.Course;
import com.ruoyi.project.system.course.service.ICourseService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 学生签到Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-22
 */
@Service
public class CourseServiceImpl implements ICourseService 
{
    @Autowired
    private CourseMapper courseMapper;

    /**
     * 查询学生签到
     * 
     * @param id 学生签到主键
     * @return 学生签到
     */
    @Override
    public Course selectCourseById(Long id)
    {
        return courseMapper.selectCourseById(id);
    }

    /**
     * 查询学生签到列表
     * 
     * @param course 学生签到
     * @return 学生签到
     */
    @Override
    public List<Course> selectCourseList(Course course)

    {
        return courseMapper.selectCourseList(course);
    }

    /**
     * 新增学生签到
     * 
     * @param course 学生签到
     * @return 结果
     */
    @Override
    public int insertCourse(Course course)
    {
        return courseMapper.insertCourse(course);
    }

    /**
     * 修改学生签到
     * 
     * @param course 学生签到
     * @return 结果
     */
    @Override
    public int updateCourse(Course course)
    {
        return courseMapper.updateCourse(course);
    }

    /**
     * 批量删除学生签到
     * 
     * @param ids 需要删除的学生签到主键
     * @return 结果
     */
    @Override
    public int deleteCourseByIds(String ids)
    {
        return courseMapper.deleteCourseByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除学生签到信息
     * 
     * @param id 学生签到主键
     * @return 结果
     */
    @Override
    public int deleteCourseById(Long id)
    {
        return courseMapper.deleteCourseById(id);
    }
}
