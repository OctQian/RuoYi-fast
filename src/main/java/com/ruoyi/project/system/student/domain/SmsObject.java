package com.ruoyi.project.system.student.domain;

import lombok.Data;

@Data
public class SmsObject {
    private String name;
    private String num;
}
