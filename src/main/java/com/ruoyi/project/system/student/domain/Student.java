package com.ruoyi.project.system.student.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 学生对象 sys_student
 * 
 * @author ruoyi
 * @date 2024-04-22
 */
public class Student extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @Excel(name = "主键")
    private Long id;

    /** 会员名字 */
    @Excel(name = "会员名字")
    private String name;

    /** 会员电话 */
    @Excel(name = "会员电话")
    private String phone;

    /** 会员类型,会员制，课程数 */
    @Excel(name = "会员类型,会员制，课程数")
    private String type;

    /** 课程数 */
    @Excel(name = "课程数")
    private Long course;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    public String getPhone()
    {
        return phone;
    }
    public void setType(String type)
    {
        this.type = type;
    }

    public String getType()
    {
        return type;
    }
    public void setCourse(Long course)
    {
        this.course = course;
    }

    public Long getCourse()
    {
        return course;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("phone", getPhone())
            .append("type", getType())
            .append("course", getCourse())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
