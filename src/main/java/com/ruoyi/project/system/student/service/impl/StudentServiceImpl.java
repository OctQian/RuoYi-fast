package com.ruoyi.project.system.student.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


import com.alibaba.fastjson.JSON;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import com.google.gson.JsonObject;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.project.system.course.domain.Course;
import com.ruoyi.project.system.course.mapper.CourseMapper;
import com.ruoyi.project.system.student.domain.SmsObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.system.student.mapper.StudentMapper;
import com.ruoyi.project.system.student.domain.Student;
import com.ruoyi.project.system.student.service.IStudentService;
import com.ruoyi.common.utils.text.Convert;
import org.springframework.transaction.annotation.Transactional;



/**
 * 学生Service业务层处理
 *
 * @author ruoyi
 * @date 2024-04-22
 */
@Service
public class StudentServiceImpl implements IStudentService {
    @Autowired
    private StudentMapper studentMapper;

    @Autowired
    private CourseMapper courseMapper;


    private static final String accessKeyId = "LTAI5t7NzMKVSmWRpR2xgVy6";
    private static final String accessKeySecret = "eGZZeMQeV5ArxGWC2D3V7CI2VVzBOu";
    private static final String regionId = "cn-hangzhou";


    /**
     * 查询学生
     *
     * @param id 学生主键
     * @return 学生
     */
    @Override
    public Student selectStudentById(Long id) {
        return studentMapper.selectStudentById(id);
    }

    /**
     * 查询学生列表
     *
     * @param student 学生
     * @return 学生
     */
    @Override
    public List<Student> selectStudentList(Student student) {
        return studentMapper.selectStudentList(student);
    }

    /**
     * 新增学生
     *
     * @param student 学生
     * @return 结果
     */
    @Override
    public int insertStudent(Student student) {
        student.setCreateTime(DateUtils.getNowDate());
        return studentMapper.insertStudent(student);
    }

    /**
     * 修改学生
     *
     * @param student 学生
     * @return 结果
     */
    @Override
    public int updateStudent(Student student) {
        student.setUpdateTime(DateUtils.getNowDate());
        return studentMapper.updateStudent(student);
    }

    /**
     * 批量删除学生
     *
     * @param ids 需要删除的学生主键
     * @return 结果
     */
    @Override
    public int deleteStudentByIds(String ids) {
        return studentMapper.deleteStudentByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除学生信息
     *
     * @param id 学生主键
     * @return 结果
     */
    @Override
    public int deleteStudentById(Long id) {
        return studentMapper.deleteStudentById(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int sign(String ids) throws Exception {
        //
        String[] strArray = Convert.toStrArray(ids);
        List<String> idList = new ArrayList<>(Arrays.asList(strArray));

        // 遍历idList
        for (String id : idList) {
            Student student = studentMapper.selectStudentById(Long.parseLong(id));
            if (student.getType().equals("课程")) {
                Long course = student.getCourse();
                if (course <= 0) {
                    throw new RuntimeException(student.getName() + "的课程数已不足,请取消改学生的勾选之后再操作");
                }
                student.setCourse(course - 1);
                studentMapper.updateStudent(student);

            }
            Course db = new Course();
            db.setStudentId(student.getId());
            db.setStudentName(student.getName());
            db.setStudentType(student.getType());

            db.setTime(new Date());

            if (student.getPhone() != null) {
//                com.aliyun.dysmsapi20170525.Client client = createClient();
//                com.aliyun.dysmsapi20170525.models.AddSmsSignRequest addSmsSignRequest = new com.aliyun.dysmsapi20170525.models.AddSmsSignRequest();
//                addSmsSignRequest.
//                        com.aliyun.teautil.models.RuntimeOptions runtime = new com.aliyun.teautil.models.RuntimeOptions();
//                try {
//                    // 复制代码运行请自行打印 API 的返回值
//                    client.addSmsSignWithOptions(addSmsSignRequest, runtime);
//                } catch (TeaException error) {
//                    // 此处仅做打印展示，请谨慎对待异常处理，在工程项目中切勿直接忽略异常。
//                    // 错误 message
//                    System.out.println(error.getMessage());
//                    // 诊断地址
//                    System.out.println(error.getData().get("Recommend"));
//                    com.aliyun.teautil.Common.assertAsString(error.message);
//                } catch (Exception _error) {
//                    TeaException error = new TeaException(_error.getMessage(), _error);
//                    // 此处仅做打印展示，请谨慎对待异常处理，在工程项目中切勿直接忽略异常。
//                    // 错误 message
//                    System.out.println(error.getMessage());
//                    // 诊断地址
//                    System.out.println(error.getData().get("Recommend"));
//                    com.aliyun.teautil.Common.assertAsString(error.message);
                DefaultProfile profile = DefaultProfile.getProfile(
                        "1092514359400943",
                        "LTAI5t7NzMKVSmWRpR2xgVy6",
                        "eGZZeMQeV5ArxGWC2D3V7CI2VVzBOu");
                IAcsClient client = new DefaultAcsClient(profile);

                SendSmsRequest request = new SendSmsRequest();
                //短信内容标题
                request.setSignName("浙江果豪体育");
                //模板id
                request.setTemplateCode("SMS_470610052");
                request.setPhoneNumbers(student.getPhone());
                //发送的信息

                SmsObject smsObject = new SmsObject();
                smsObject.setName(student.getName());
                smsObject.setNum(student.getCourse().toString());

                String jsonString = JSON.toJSONString(smsObject);
                System.out.println(jsonString);
                request.setTemplateParam(jsonString);

                try {
                    SendSmsResponse response = client.getAcsResponse(request);
                    System.out.println(response);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            int i = courseMapper.insertCourse(db);

        }


        return 1;

    }

}




