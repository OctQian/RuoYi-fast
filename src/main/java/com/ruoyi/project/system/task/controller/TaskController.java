package com.ruoyi.project.system.task.controller;

import java.util.List;

import com.ruoyi.project.system.customer.domain.Customer;
import com.ruoyi.project.system.customer.service.ICustomerService;
import com.ruoyi.project.system.user.domain.User;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.task.domain.Task;
import com.ruoyi.project.system.task.service.ITaskService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 询价单Controller
 *
 * @author ruoyi
 * @date 2024-01-10
 */
@Controller
@RequestMapping("/system/task")
public class TaskController extends BaseController {
    private String prefix = "system/task";

    @Autowired
    private ITaskService taskService;

    @Autowired
    private ICustomerService iCustomerService;

    @RequiresPermissions("system:task:view")
    @GetMapping()
    public String task() {
        return prefix + "/task";
    }

    /**
     * 查询询价单列表
     */
    @RequiresPermissions("system:task:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Task task) {
        startPage();
        List<Task> list = taskService.selectTaskList(task);
        return getDataTable(list);
    }

    /**
     * 导出询价单列表
     */
    @RequiresPermissions("system:task:export")
    @Log(title = "询价单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Task task) {
        List<Task> list = taskService.selectTaskList(task);
        ExcelUtil<Task> util = new ExcelUtil<Task>(Task.class);
        return util.exportExcel(list, "询价单数据");
    }

    /**
     * 新增询价单
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 新增保存询价单
     */
    @RequiresPermissions("system:task:add")
    @Log(title = "询价单", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(Task task) {
        User sysUser = getSysUser();
        task.setDeptId(sysUser.getDeptId());
        task.setUserId(getUserId());
        task.setCreateBy(getLoginName());
        Long customerId = task.getCustomerId();
        Customer customer = iCustomerService.selectCustomerByCustomerId(customerId);
        task.setCustomerName(customer.getName());
        return toAjax(taskService.insertTask(task));
    }

    /**
     * 修改询价单
     */
    @RequiresPermissions("system:task:edit")
    @GetMapping("/edit/{taskId}")
    public String edit(@PathVariable("taskId") Long taskId, ModelMap mmap) {
        Task task = taskService.selectTaskByTaskId(taskId);
        mmap.put("task", task);
        return prefix + "/edit";
    }

    /**
     * 修改保存询价单
     */
    @RequiresPermissions("system:task:edit")
    @Log(title = "询价单", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Task task) {
        task.setUpdateBy(getLoginName());
        Customer customer = iCustomerService.selectCustomerByCustomerId(task.getCustomerId());
        task.setCustomerName(customer.getName());
        return toAjax(taskService.updateTask(task));
    }

    /**
     * 删除询价单
     */
    @RequiresPermissions("system:task:remove")
    @Log(title = "询价单", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(taskService.deleteTaskByTaskIds(ids));
    }

}
