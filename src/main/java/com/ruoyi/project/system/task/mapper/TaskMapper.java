package com.ruoyi.project.system.task.mapper;

import java.util.List;
import com.ruoyi.project.system.task.domain.Task;

/**
 * 询价单Mapper接口
 * 
 * @author ruoyi
 * @date 2024-01-10
 */
public interface TaskMapper 
{
    /**
     * 查询询价单
     * 
     * @param taskId 询价单主键
     * @return 询价单
     */
    public Task selectTaskByTaskId(Long taskId);

    /**
     * 查询询价单列表
     * 
     * @param task 询价单
     * @return 询价单集合
     */
    public List<Task> selectTaskList(Task task);

    /**
     * 新增询价单
     * 
     * @param task 询价单
     * @return 结果
     */
    public int insertTask(Task task);

    /**
     * 修改询价单
     * 
     * @param task 询价单
     * @return 结果
     */
    public int updateTask(Task task);

    /**
     * 删除询价单
     * 
     * @param taskId 询价单主键
     * @return 结果
     */
    public int deleteTaskByTaskId(Long taskId);

    /**
     * 批量删除询价单
     * 
     * @param taskIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTaskByTaskIds(String[] taskIds);
}
