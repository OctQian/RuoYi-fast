package com.ruoyi.project.system.task.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.system.task.mapper.TaskMapper;
import com.ruoyi.project.system.task.domain.Task;
import com.ruoyi.project.system.task.service.ITaskService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 询价单Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-01-10
 */
@Service
public class TaskServiceImpl implements ITaskService 
{
    @Autowired
    private TaskMapper taskMapper;

    /**
     * 查询询价单
     * 
     * @param taskId 询价单主键
     * @return 询价单
     */
    @Override
    public Task selectTaskByTaskId(Long taskId)
    {
        return taskMapper.selectTaskByTaskId(taskId);
    }

    /**
     * 查询询价单列表
     * 
     * @param task 询价单
     * @return 询价单
     */
    @Override
    public List<Task> selectTaskList(Task task)
    {
        return taskMapper.selectTaskList(task);
    }

    /**
     * 新增询价单
     * 
     * @param task 询价单
     * @return 结果
     */
    @Override
    public int insertTask(Task task)
    {
        task.setCreateTime(DateUtils.getNowDate());
        return taskMapper.insertTask(task);
    }

    /**
     * 修改询价单
     * 
     * @param task 询价单
     * @return 结果
     */
    @Override
    public int updateTask(Task task)
    {
        task.setUpdateTime(DateUtils.getNowDate());
        return taskMapper.updateTask(task);
    }

    /**
     * 批量删除询价单
     * 
     * @param taskIds 需要删除的询价单主键
     * @return 结果
     */
    @Override
    public int deleteTaskByTaskIds(String taskIds)
    {
        return taskMapper.deleteTaskByTaskIds(Convert.toStrArray(taskIds));
    }

    /**
     * 删除询价单信息
     * 
     * @param taskId 询价单主键
     * @return 结果
     */
    @Override
    public int deleteTaskByTaskId(Long taskId)
    {
        return taskMapper.deleteTaskByTaskId(taskId);
    }
}
